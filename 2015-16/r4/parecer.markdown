#Parecer:

O trabalho � excelente e gerou conhecimento sobre uma �rea de pesquisa cr�tica em nossa 
regi�o, onde o com�rcio popular e a explora��o industrial de fitoter�picos � muito presente, 
fazendo parte integral de nossa cultura. Chama a aten��o as discrep�ncias dos resultados 
com rela��o aos par�metros aceit�veis pela Farmacop�ia Brasileira, o que traz preocupa��o 
com a qualidade dos fitoter�picos que nossa popula��o consome. Isso suscita a grande 
import�ncia da capacita��o e educa��o dos produtores e comerciantes populares destes 
produtos, al�m do esclarecimento da popula��o em geral. A id�ia amplamente aceita de que 
fitoter�picos nunca t�m efeitos adversos torna-se perigosa neste contexto. Seria 
importante que o projeto gerasse produtos (publica��es).